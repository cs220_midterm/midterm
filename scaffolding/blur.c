#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "ppm_io.h"

double sq(double num) {
  return num*num;
}

double ** gaussian(double sigma) {

  const double PI = 3.14159265358979323846264338;
  
  int N = sigma*10;
  if (N%2 == 0) {
    N++;
  }

  double **G = malloc(sizeof(double*) * N);
  for (int i = 0; i < N; i++) {
    G[i] = malloc(sizeof(double) * N);
  }
  
  const double c = 1.0/(2.0*PI*sq(sigma));
  double temp;
  int dx, dy;
  
  //i = rows, j = col
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      dx = (N/2) - i;
      dy = (N/2) - j;
      temp = -((sq(dx) + sq(dy))/(2*sq(sigma)));
      G[i][j] = c*exp(temp);
      //      printf("%f ", G[i][j]);
    }
    // printf("\n");
  }

  return G;
}


int filterResponse(double ** g, const Image * img, int r, int c, char t,
		   double sigma) {

  double sum = 0;
  double weight_sum = 0;

  int N = sigma*10;
  N++;
  if (N%2 == 0) {
    N++;
  }

  int imr_s, imc_s, filr_s, filc_s; 
  int imr_e, imc_e, filr_e, filc_e; 
  
  if ((N/2) > r) {
    imr_s = 0;
    filr_s = r;
   }
  else {
    filr_s = 0;
    imr_s = r - (N/2);
  }

  if ((N/2) > c) {
    imc_s = 0;
    filc_s = c;
   }
  else {
    filc_s = 0;
    imc_s = c - (N/2);
  }

  if ((N/2) > img->y - r){
    filr_e = img->y - r;
    imr_e = img->y;
  }
  else {
    filr_e = N;
    imr_e = r + (N/2);
  }

  if ((N/2) > img->x - c){
    filc_e = img->x - c;
    imc_e = img->x;
  }
  else {
    filc_e = N;
    imc_e = c + (N/2);
  }

  int R = filr_e - filr_s;
  int C = filc_e - filc_s;

  if (t == 'r') {
    for (int i = 0; i < R; i++) {
      for (int j = 0; j < C; j++) {
        sum += g[i][j]*img->data[((imr_s + i) * img->x) + (imc_s + j)].r;
	weight_sum += g[i][j];
      }
    }
   }

   if (t == 'b') {
    for (int i = 0; i < R; i++) {
      for (int j = 0; j < C; j++) {
        sum += g[i][j]*img->data[((imr_s + i) * img->x) + (imc_s + j)].b;
	weight_sum += g[i][j];
      }
    }
   }

   if (t == 'g') {
    for (int i = 0; i < R; i++) {
      for (int j = 0; j < C; j++) {
        sum += g[i][j]*img->data[((imr_s + i) * img->x) + (imc_s + j)].g;
	weight_sum += g[i][j];
      }
    }
   } 

  int val = sum/weight_sum;
 
  if (val > 255) {
    val = 255;
  }

  return val;
 
}

Image * blur(Image * im, double sigma) {

  double **g = gaussian(sigma);
 
  for (int row = 0; row < im->y; row++) {
    for (int col = 0; col < im->x; col++) {
      im->data[(row * im->x) + col].r = filterResponse(g,im,row,col,'r',sigma);
      im->data[(row * im->x) + col].b = filterResponse(g,im,row,col,'b',sigma);
      im->data[(row * im->x) + col].g = filterResponse(g,im,row,col,'g',sigma);
    }
  }  
  
  return im;
}

Image * grayscale(Image * img) {

  double intense;  
   for (int r = 0; r < img->y; r++) {
    for (int c = 0; c < img->x; c++) {
      intense = ((img->data[(r * img->x) + c].r)*0.3) +
	((img->data[(r * img->x) + c].g)*0.59) +
	((img->data[(r * img->x) + c].b)*0.11);
      img->data[(r * img->x) + c].r = intense;
      img->data[(r * img->x) + c].g = intense;
      img->data[(r * img->x) + c].b = intense; 
    }
  }

   return img;
  
}

Image * edgeDetection (Image * img, double sigma, double thresh) {

  int rows = img->y;
  int cols = img->x;
  img  = grayscale(img);
  img  = blur(img, sigma);
  
  int **intensex = malloc(sizeof(int*) * rows);
  for (int i = 0; i < rows; i++) {
    intensex[i] = malloc(sizeof(int) * cols);
  }

  int **intensey = malloc(sizeof(int*) * rows);
  for (int i = 0; i < rows; i++) {
    intensey[i] = malloc(sizeof(int) * cols);
  }

  double **intenseMag = malloc(sizeof(double*) * rows);
  for (int i = 0; i < rows; i++) {
    intenseMag[i] = malloc(sizeof(double) * cols);
  }
  
  printf("no prob\n");

  for (int i = 1; i < img->y - 1; i++) {
    for (int j = 1; j < img->x-1; j++) {
      intensex[i][j] = (img->data[((i+1) * img->x) + j].r -
			img->data[((i-1) * img->x) + j].r)/2;
      
      intensey[i][j] = (img->data[(i * img->x) + (j+1)].r -
			img->data[(i * img->x) + (j-1)].r)/2;

      intenseMag[i][j] = sqrt(sq(intensex[i][j]) + sq(intensey[i][j]));
    }
   }

    printf("no prob2\n");

   for (int i = 1; i < img->y - 1; i++) {
    for (int j = 1; j < img->x-1; j++) {

      if (intenseMag[i][j] > thresh) {
        img->data[(i * img->x) + j].r = 0;
        img->data[(i * img->x) + j].g = 0;
        img->data[(i * img->x) + j].b = 0;
      }
      else {
        img->data[(i * img->x) + j].r = 255;
        img->data[(i * img->x) + j].g = 255;
        img->data[(i * img->x) + j].b = 255;
      }
    }
   }
  return img;
}
