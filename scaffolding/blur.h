#ifndef BLUR_H
#define BLUR_H

#include <math.h>
#include "ppm_io.h"

double sq(double num);

double ** gaussian(double sigma);

int filterResponse(double ** g, Image * img, int r, int c, char t);

Image * blur(Image * im, double sigma);

Image * grayscale(Image * img);

Image * edgeDetection(Image * img, double sigma, double thresh);

#endif
