// Soo Hyun Lee (slee387)
// Eliza Cohn (ecohn4)
// imageManip.h
// 601.220, Spring 2019

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdlib.h>
#include <math.h>
#include "ppm_io.h"

// This function swaps the R, G, B values to produce different shades of the image.
Image * swap(Image * im);

// This helper function clamps the rgb values to [0,255].
int saturation_check(char input, double level);

// This function adjusts the brightness of the image based on user input.
Image * bright(Image *im, double level);

// This function inverts the colors of the image.
Image * invert(Image * im);

// This function changes the image to grayscale.
Image * grayscale(Image * img);

// This function crops the image based on user input.
Image * crop(Image * im, int topCol, int topRow, int botCol, int botRow);

// This function returns the square of the value passed in
double sq(double num);

// For a given sigma value, compute and return its Gaussian matrix
double ** gaussian(double sigma);

// This is a helper function for blur. For each pixel passed in, it computes
// its blurred value by executing the matrix convolution at this point
int filterResponse(double ** g, Image * img, int r, int c, char t);

// This function blurs the image
Image * blur(Image * im, double sigma);

// This function produces the edge detected image
Image * edgeDetection(Image * img, double sigma, double thresh);

#endif
