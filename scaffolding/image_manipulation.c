#include <stdlib.h>
#include "ppm_io.h"
#include <math.h>

Image * swap(Image * im) {

	unsigned char red, green, blue;
	for (int rows = 0; rows < im->y; rows++) {

		for (int cols = 0; cols < im->x; cols++) {
			red = im->data[(rows * im->x + cols)].r;
			green = im->data[(rows * im->x + cols)].g;
			blue = im->data[(rows * im->x + cols)].b;

			im->data[(rows * im->x + cols)].r = green;
			im->data[(rows * im->x + cols)].g = blue;
			im->data[(rows * im->x + cols)].b = red;

		}
	}

	return im;
}

int saturation_check(unsigned char input, int level) {

	int calc = input + level;
	if (calc > 255) {
		return 255;
	} else if (calc < 0) {
		return 0;
	} else {
		return calc;
	}
}


Image * bright(Image * im, int level) {

	Image * retIm = malloc(sizeof(Image));
	Pixel * pix = malloc(sizeof(Pixel) * im->x * im->y);
	retIm->data = pix;
	retIm->x = im->x;
	retIm->y = im->y;

	for (int rows = 0; rows < im->y; rows++) {

		for (int cols = 0; cols < im->x; cols++) {

			retIm->data[(rows * im->x + cols)].r = saturation_check(im->data[(rows * im->x + cols)].r, level);
			retIm->data[(rows * im->x + cols)].g = saturation_check(im->data[(rows * im->x + cols)].g, level);
			retIm->data[(rows * im->x + cols)].b = saturation_check(im->data[(rows * im->x + cols)].b, level);
		}
	}

	return retIm;
}

// Image * bright(Image *im, int level) {

// 	for (int rows = 0; rows < im->y; rows++) {

// 		for (int cols = 0; cols < im->x; cols++) {

// 			im->data[(rows * im->x + cols)].r = saturation_check(im->data[(rows * im->x + cols)].r, level);
// 			im->data[(rows * im->x + cols)].g = saturation_check(im->data[(rows * im->x + cols)].g, level);
// 			im->data[(rows * im->x + cols)].b = saturation_check(im->data[(rows * im->x + cols)].b, level);
// 		}
// 	}

// 	return im;
// }

Image * invert(Image * im) {

	// Image * retIm = malloc(sizeof(im));
	// retIm->x = im->x;
	// retIm->y = im->y;

	for (int rows = 0; rows < im->y; rows++) {

		for (int cols = 0; cols < im->x; cols++) {
			im->data[(rows * im->x + cols)].r = 255 - im->data[(rows * im->x + cols)].r;
			im->data[(rows * im->x + cols)].g = 255 - im->data[(rows * im->x + cols)].g;
			im->data[(rows * im->x + cols)].b = 255 - im->data[(rows * im->x + cols)].b;
		}
	}

	return im;
}

Image * grayscale(Image * img) {

	double intense;  
    for (int r = 0; r < img->y; r++) {
    	for (int c = 0; c < img->x; c++) {

      		intense = ((img->data[(r * img->x) + c].r)*0.3) +
	   		((img->data[(r * img->x) + c].g)*0.59) +
	   		((img->data[(r * img->x) + c].b)*0.11);

      		img->data[(r * img->x) + c].r = intense;
      		img->data[(r * img->x) + c].g = intense;
      		img->data[(r * img->x) + c].b = intense; 
    	}
  	}

   return img;
  
}

Image * crop(Image * im, int topCol, int topRow, int botCol, int botRow) {

	int newRows = botRow - topRow;
	int newCols = botCol - topCol;

	Image * retIm = malloc(sizeof(Image));
	Pixel * pix = malloc(sizeof(Pixel) * newRows * newCols);
	retIm->data = pix;
	retIm->x = newCols;
	retIm->y = newRows;

	for (int rows = 0; rows < newRows; rows++) {

		for (int cols = 0; cols < newCols; cols++) {

			retIm->data[(rows * newCols + cols)].r = im->data[((topRow + rows) * im->x + topCol + cols)].r;
			retIm->data[(rows * newCols + cols)].g = im->data[((topRow + rows) * im->x + topCol + cols)].g;
			retIm->data[(rows * newCols + cols)].b = im->data[((topRow + rows) * im->x + topCol + cols)].b;

		}
	}

	return retIm;
	
}
