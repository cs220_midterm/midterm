#ifndef IMAGE_MANIPULATION_H
#define IMAGE_MANIPULATION_H

#include <stdlib.h>
#include <math.h>
#include "ppm_io.h"


Image * swap(Image * im);

int saturation_check(char input, int level);

Image * bright(Image *im, int level);

Image * invert(Image * im);

Image * grayscale(Image * img);

Image * crop(Image * im, int topCol, int topRow, int botCol, int botRow);


#endif
