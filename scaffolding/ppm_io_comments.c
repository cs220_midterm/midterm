// Eliza Cohn
// ecohn4
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>

// typedef struct _pixel {
//     unsigned char red,green,blue;
// } Pixel;

// typedef struct _image {
//     int x, y;
//     Pixel *data;
// } Image;

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

    // check that fp is not NULL
    assert(fp); 
    Image *img;
    char p;
    int six, rgb;

    // p = fgetc(fp);
    // if (p != 'P') {
    //     printf("Invalid image format\n");
    // } else {
        
    //     p = fgetc(fp);

    //     if (p != '6') {
    //         printf("Invalid image format\n");
    //     }
    // }

    //Checking if the format is P6
    if (fscanf(fp, " %c%d", &p, &six) == 2){
        if(p != 'P') {
            printf("error");
        }

        if (six != 6) {
            printf("error");
        }
    }

        // char buffer[50];
    // fgets(buffer, 50, fp);


    // if (buffer[0] != '#') {
    //     int first;
    //     sscanf(buffer, "%d", &first);
    //     int second;
    //     fgets(buffer, 50, fp);
    //     sscanf(buffer, "%d", &second);
    //     img->x = first;
    //     img->y = second;
    //     //add error here 
    // } else {
    //     p = fgetc(fp);
    //     while (p != '\n') {
    //         p = fgetc(fp);
    //     }
    //     if (fscanf(fp, " %d %d", &img->x, &img->y) != 2) {
    //         printf("%d %d", img->x, img->y);
    //         printf("Invalid size of image\n");
    //         return NULL; //check 
    //     }
    // }

    //Check for comments & do nothing about them
    //Just moving the pointers 
    p = fgetc(fp);
    // printf("1: %c", p);
    p = fgetc(fp);
    //    printf("2: %c", p);

    if (p == '#') {
        while (p != '\n') {
            p = fgetc(fp);
        }
    }

    // while (p == '#') {
    //     printf("3: %c", p);
    //     while (p != '\n') {
    //         p = fgetc(fp);
    //         printf("4: %c", p);
    //     }
    //     p = fgetc(fp);
    // }

    // printf("5: %c", p);

    // while (p == '\n' || p == ' ') {
    //     p = fgetc(fp);
    //     printf("1: %c", p);
    //     while (p == '#') {
    //         if ()


    //         while (fgetc(fp) != '\n') {
    //            printf("2: %c", p);
    //            p = fgetc(fp);
    //         }
    //     }

    // }

    // while (p == '#') {
    //     while (fgetc(fp) != '\n') {
    //         p = fgetc(fp);
    //     }
    // }


    //Allocate memory for image
    img = (Image *)malloc(sizeof(Image));
    if (!img) {
        printf("Memory allocation failed\n");
        return NULL; //Copied from demo 
    }

    // while (p == '\n' || p == ' ') {
    //     p = fgetc(fp);
    //     printf("%c", p);

    // }
    //printf("%c", p);


    //Cols & rows not read 
    if (fscanf(fp, " %d%d", &img->x, &img->y) != 2) {
        printf("%d %d", img->x, img->y);
        printf("Invalid size of image\n");
        return NULL; //check 
    }

    if (fscanf(fp, " %d", &rgb) != 1) {
        printf("Invalid rgb value.\n");
        return NULL; //check 
    }

    p = fgetc(fp);
    //    printf("checking %c", p);

    if (rgb != 255) {
        printf("Not an rgb value.\n");
        return NULL; //check 
    }


    //HANDLE: while (fgetc(fp) != '\n')

    Pixel * pix = malloc(sizeof(Pixel) * img->x * img->y);
    if (!pix) {
        printf("Pixel array allocation failed.\n");
        free(img);
        return NULL;
    }

    img->data = pix;

    fread(img->data, 3 * img->x, img->y, fp);
    if (feof(fp)) {
        printf("pointer got to the end\n");
    }

    //Now read pixel data from file 

    //if (fread())

    //read pixel data from file
    // if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
    //      fprintf(stderr, "Error loading image '%s'\n", filename);
    //      exit(1);
    // }



  //fill Pixel array by setting all Pixels to same color
  // Pixel my_color = {0, 127, 255}; //the color for most pixels
  // Pixel white = {255, 255, 255}; //the color for a few pixels
  // for (int r = 0; r < img->y; r++) {
  //   for (int c = 0; c < img->x; c++) {
  //     if ((r + c) % 8 != 0) { 
  //       img->data[(r * img->x) + c] = my_color;
  //     } else {
  //       img->data[(r * img->x) + c] = white;
  //     }
  //   }
  // }


  //read in cols and rows
  //check 255
  //send find using fgetc to the img pointer

  fclose(fp);
  return img;  //TO DO: replace this stub

}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  if (!im) {
      printf("Ah");
  }

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->x, im->y);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->x * im->y, fp);

  if (num_pixels_written != im->x * im->y) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;

  // return 1;
}

/*
int main() {
    FILE *fp = fopen("nika.ppm", "rb");
    Image * temp = read_ppm(fp);

    if (!temp) {
      printf("here");
    }

    FILE *fp2 = fopen("nika2.ppm", "wb");
    int z = write_ppm(fp2, temp);

    printf("are we here? %d", z);

    return 0;
    }*/

